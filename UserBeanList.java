import java.util.ArrayList;

public class UserBeanList {
    public static ArrayList<UserBean> getUserBeanList() {
        ArrayList<UserBean> userBeans = new ArrayList<UserBean>();
        ArrayList<User> userList = UserFactory.getUserList();

        //Create user bean
        for (User u : userList) {
            UserBean ub = new UserBean();
            ub.setGivenName(u.givenName());
            ub.setFamilyName(u.familyName());
            ub.setUserID(u.userID());
            userBeans.add(ub);
        }

        //Add won listing info
        ArrayList<Item> itemList = ItemFactory.getItemList();
        for (Item i : itemList) {
            if (i.getStatus() == ItemStatus.COMPLETED) {
                //Make item bean
                ItemBean itemBean = new ItemBean();
                itemBean.setItemID(i.getItemID());
                itemBean.setTitle(i.getTitle());
                itemBean.setEndTime(Utilities.df.format(i.getEndTime()));
                itemBean.setPrice(Utilities.formatPrice(i.getTopBidValue()));

                //Add bean to user
                UserBean winningUser = userBeans.get(i.getTopBidder());
                winningUser.addToItemBeanList(itemBean);
            }
        }

        return userBeans;
    }

}


