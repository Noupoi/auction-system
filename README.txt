This application was created to the specification in "specification.pdf".

To compile and run project:
	- Compile using: "javac -cp "lib/*" *.java"
	- Start server with: "java -cp "lib/*"; Server"
	- Start client with: "java -cp "lib/*"; Client"

Notes
	- The AuctionDebugTools class has methods that can be useful when testing the system.
	- The Item and User classes are within the ItemFactory and UserFactory .java files.
	- Filtering the listings displayed in the client is done with the 'Search' button.
	- When generating reports, users that have not won any items will not have a table of items won on their page.
	- The server will load persistent data on startup if found. Data is auto-saved every 5 mins.
	
	- PasswordHash class is from https://crackstation.net/hashing-security.htm
	- DataTimePicker class is from http://web.archive.org/web/20111118105909/http://wiki.java.net/twiki/bin/view/Javadesktop/JXDateTimePicker?TWIKISID=cc05dea1611ad043796cd67ed476d711;skin=print
	- SwingX library is from https://java.net/projects/swingx/downloads
	- Quartz scheduling library is from http://quartz-scheduler.org/
	- JasperReports library is from http://community.jaspersoft.com/project/jasperreports-library
	- AuctionSystemSubreport was based on test_jasper.jrxml from: http://java-bytes.blogspot.co.uk/2009/06/jasper-reports-example.html
	- lambdaj library is from https://code.google.com/p/lambdaj/
	- commons-io-2.4 is from http://commons.apache.org/proper/commons-io/
	- forms_rt is needed for and included with IntelliJ's GUI builder.
	- The remaining .jar files came with JasperReports and are possible dependencies.

Completed extensions
	- Use Java Sockets in the Communication Layer to allow Clients to interact with the Server across a network.
	- Multi-threaded thread pooled server. Allows multiple clients to be serviced at once.
	- Hash and salt stored passwords to improve security.
	- Users can withdraw listings. Withdrawing a listing with bid higher than reserve price results in penalty point. (Those with 3 or more points cannot create listings.)
	- Reports can be exported to various file formats including pdf and docx. Reports can also be printed from server GUI.
