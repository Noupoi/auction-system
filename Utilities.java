import javax.swing.*;
import javax.swing.text.NumberFormatter;
import java.net.Socket;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Locale;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by Daniel on 30/04/2014.
 */

public class Utilities {
    public static Logger globalLogger = null;
    public static DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    static NumberFormat ukFormat = NumberFormat.getCurrencyInstance(Locale.UK);
    private static ConcurrentSkipListMap<Integer, Message> userMessageMap = new ConcurrentSkipListMap<Integer, Message>();
    public static Server server;

    public static void init() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        initLogging();
    }

    public static void serverInit(Server sv) {
        init();
        server = sv;
    }

    public static Server getServer() {
        return server;
    }

    public static void initLogging() {
        if (globalLogger == null) {
            globalLogger = Logger.getLogger("AuctionSystem");

            ConsoleHandler ch = new ConsoleHandler();
            ch.setFormatter(new SimpleFormatter());

            globalLogger.addHandler(ch);
            ch.setLevel(Level.ALL);

            globalLogger.setLevel(Level.ALL);

        }
    }

    public static NumberFormatter getPriceFormatter() { //Used to format currency fields
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.UK);
        format.setMaximumFractionDigits(2);

        NumberFormatter formatter = new NumberFormatter(format);
        formatter.setMinimum(0.0);
        formatter.setMaximum(1000000.0);
        formatter.setAllowsInvalid(false);
        formatter.setOverwriteMode(true);
        return formatter;
    }

    public static String formatPrice(int price) {
        return ukFormat.format((double) price / 100);
    }

    public static ConcurrentSkipListMap getUserMessageMap() {
        return userMessageMap;
    }

    public static Message getMostRecentMessage(int userID) {
        return userMessageMap.get(userID);
    }
}

class MessageList { //Used to store outgoing messages
    LinkedList<Message> list = new LinkedList<Message>();

    synchronized public boolean add(Message m) {
        boolean result = list.add(m);
        notifyAll();
        return result;
    }

    synchronized public Message get(Socket s) { //Used by OutgoingWorker to get messages
        boolean found = false;
        Iterator it = list.iterator();
        Message nextMsg = null;

        while (it.hasNext()) {
            nextMsg = (Message) it.next();
            //When initiating contact as client, the first message
            // will not have a source socket as it is not a reply
            if (nextMsg.getSocket() == s || nextMsg.getSocket() == null) {
                found = true;
                it.remove(); //Remove from queue
                break;
            }
        }

        if (found) {
            return nextMsg;
        } else {
            try {
                wait(); //Block until list is updated
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return null;

    }
}

class MessageListener extends Thread {
    Comms comms;
    ExecutorService threadPool = Executors.newFixedThreadPool(100);
    private final Logger logger = Utilities.globalLogger.getLogger(Comms.class.getName());
    boolean isServer;
    ClientLogin cl;
    Server sv;

    MessageListener(Comms comms, ClientLogin cl) {
        this.comms = comms;
        isServer = false;
        this.cl = cl;
    }

    MessageListener(Comms comms, Server sv) {
        this.comms = comms;
        isServer = true;
        this.sv = sv;
    }

    @Override
    public void run() {
        logger.config("MessageListener started");
        while (true) {
            if (isServer) {
                ClientMessage msg = (ClientMessage) comms.receiveMessage();

                Utilities.getUserMessageMap().put(msg.userID, msg); //Store most recent message from user
                threadPool.execute(new ServerMessageRunner(msg));
            } else {
                threadPool.execute(new ClientMessageRunner(comms.receiveMessage()));
            }

        }

    }

    class ServerMessageRunner extends Thread {
        ClientMessage msg;

        ServerMessageRunner(Message msg) {
            this.msg = (ClientMessage) msg;
        }

        @Override
        public void run() {
            msg.setServer(sv);
            msg.execute();

        }

    }

    class ClientMessageRunner extends Thread {
        ServerMessage msg;

        ClientMessageRunner(Message msg) {
            this.msg = (ServerMessage) msg;
        }

        @Override
        public void run() {
            msg.setClient(cl);
            msg.execute();
        }
    }

}




