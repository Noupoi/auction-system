import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static ch.lambdaj.Lambda.*;
import static org.hamcrest.Matchers.*;

/**
 * Created by Daniel on 15/05/2014.
 */
public class FilterTest {
    public static void main(String[] args) {
        AuctionDebugTools.createTestListings();

        List<Item> items = ItemFactory.getItemList();

        System.out.println("Default list: " + items);

        //Search by seller id
        List<Item> itemsBySeller0 = filter(having(on(Item.class).getVendorID(),equalTo(0) ), items);
        System.out.println("Items from seller 0: " + itemsBySeller0);

        //By item type
        List<Item> itemOfTypeOther = filter(having(on(Item.class).getCategory(),equalTo(ItemType.OTHER) ), items);
        System.out.println("Items of type Other: " + itemOfTypeOther);

        //By date
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.DAY_OF_MONTH, -1);
        List<Item> itemsStartedAfterYesterday =
                filter(having(on(Item.class).getStartTime(),greaterThan(gc.getTimeInMillis()) ), items);
        System.out.println("Items started after yesterday: " + itemsStartedAfterYesterday);

        //By status
        List<Item> activeItems = filter(having(on(Item.class).getStatus(),equalTo(ItemStatus.ACTIVE) ), items);
        System.out.println("Pending items: " + activeItems);

        //By bidder
        List<Item> itemsBidOnBy0 = filter(having(on(Item.class)
                .isBidder(0),equalTo(true) ), items);
        System.out.println("Items bid on by user 0: " + itemsBidOnBy0);
    }

}
