import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

/**
 * Created by Daniel on 17/04/2014.
 */
public abstract class Comms {
    int port = 20557;
    LinkedBlockingQueue<Message> incomingMessageQueue = new LinkedBlockingQueue<Message>();
    MessageList outgoingMessageQueue = new MessageList();
    protected final Logger logger = Utilities.globalLogger.getLogger(Comms.class.getName());

    public Comms() {
        //Create logger
        logger.config("Comms logger initiated");

    }

    void sendMessage(Message msg) {
        outgoingMessageQueue.add(msg);
    }

    Message receiveMessage() {
        Message msg = null;
        try {
            msg = incomingMessageQueue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return msg;
    }


    abstract class CommsWorker extends Thread {
        Socket connection;
        String label = null;

        CommsWorker(Socket connection) {
            this.connection = connection;
        }
    }

    class IncomingWorker extends CommsWorker {
        ObjectInputStream ois;

        IncomingWorker(Socket connection) {
            super(connection);
        }

        IncomingWorker(Socket connection, String label) {
            super(connection);
            this.label = label;
        }

        @Override
        public void run() {
            try {
                BufferedInputStream bis = new BufferedInputStream(connection.getInputStream());
                ois = new ObjectInputStream(bis);

                logger.config(label + " started");

                while (true) {

                    //Listen for incoming messages
                    logger.config(label + " waiting for message");
                    Object input = ois.readObject();
                    logger.config(label + " finished reading");

                    if (input != null) {
                        logger.config(label + " received message");
                        Message inputMessage = (Message) input;
                        inputMessage.setSocket(connection);
                        incomingMessageQueue.add(inputMessage);
                    }
                }

            } catch (SocketException s) {
                logger.warning("Connection has been lost!");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

    }

    class OutgoingWorker extends CommsWorker {
        ObjectOutputStream oos;

        OutgoingWorker(Socket connection) {
            super(connection);
        }

        OutgoingWorker(Socket connection, String label) {
            super(connection);
            this.label = label;
        }

        @Override
        public void run() {
            try {
                BufferedOutputStream bos = new BufferedOutputStream(connection.getOutputStream());
                oos = new ObjectOutputStream(bos);
                oos.flush();

                logger.config(label + " started");

                while (true) {
                    //Check for outgoing messages to be sent over this socket
                    logger.config(label + " checking for outgoing messages");
                    Message msgOut = outgoingMessageQueue.get(connection);

                    oos.reset(); //Prevent caching of objects

                    if (msgOut != null) {
                        oos.writeObject(msgOut);
                        oos.flush();
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    oos.flush();
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}


class ServerComms extends Comms {
    static ServerSocket serverSocket;

    ServerComms() {
        try {
            serverSocket = new ServerSocket(port);

            //Start new dispatcher
            (new ServerCommsDispatcher()).start();
            logger.config("Server dispatcher started");

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    class ServerCommsDispatcher extends Thread {
        private ExecutorService threadPool = Executors.newFixedThreadPool(100);

        public void run() {
            while (true) {
                try {
                    Socket connection = serverSocket.accept();
                    logger.info("Server accepted connection");

                    threadPool.execute(new IncomingWorker(connection, "ServerIncomingWorker"));
                    threadPool.execute(new OutgoingWorker(connection, "ServerOutgoingWorker"));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

class ClientComms extends Comms {
    String host = "localhost";
    int userID = -1;

    ClientComms() {
        //Start worker thread
        (new ClientCommsDispatcher()).start();
    }

    ClientComms(String host){
        this.host = host;
        (new ClientCommsDispatcher()).start();
    }

    class ClientCommsDispatcher extends Thread {
        @Override
        public void run() {
            try {
                InetAddress address = InetAddress.getByName(host);
                Socket connection = new Socket(address, port);

                (new IncomingWorker(connection, "ClientIncomingWorker")).start();
                (new OutgoingWorker(connection, "ClientOutgoingWorker")).start();

            } catch (ConnectException c) {
                JOptionPane.showMessageDialog(null,
                        "Could not establish connection to server! \nExiting...",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                System.exit(-1);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

