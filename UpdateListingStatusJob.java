import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class UpdateListingStatusJob implements Job {
    int itemID;

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.out.println("Checking status of item "+ itemID);
        Utilities.getServer().refreshItemStatus(itemID); //Refresh item status
    }
}
