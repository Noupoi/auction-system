/**
 * Created by Daniel on 04/05/2014.
 */
public abstract class ClientMessage extends Message {

    public final int userID;

    public ClientMessage(int userID) {
        this.userID = userID;
    }
}

class cl_Login extends ClientMessage {
    public final char[] password;

    cl_Login(int userID, char[] password) {
        super(userID);
        this.password = password;
    }

    @Override
    public void execute() {
        sv.login(this);
    }
}

class cl_Register extends ClientMessage {
    String givenName, familyName;
    String password;

    cl_Register(String givenName, String familyName, String password) {
        super(-1);
        this.givenName = givenName;
        this.familyName = familyName;
        this.password = password;
    }

    @Override
    public void execute() {
        sv.register(this);
    }

}

class cl_GetPenaltyPoints extends ClientMessage{

    public cl_GetPenaltyPoints(int userID) {
        super(userID);
    }

    @Override
    public void execute() {
        sv.getPenaltyPoints(this);
    }
}

class cl_GetListings extends ClientMessage {
    cl_GetListings(int userID) {
        super(userID);
    }

    @Override
    public void execute() {
        sv.getListings(this);
    }
}

class cl_GetFilteredListings extends ClientMessage {
    int userID;
    //Info for search
    int itemID;
    int sellerID;
    int bidderID;
    ItemType category;
    ItemStatus status;
    long startTimeStart;
    long startTimeEnd;
    long endTimeStart;
    long endTimeEnd;
    boolean startTimeEnabled;
    boolean endTimeEnabled;
    boolean categoryEnabled;
    boolean statusEnabled;

    public cl_GetFilteredListings(int userID,
                                  int itemID,
                                  int sellerID,
                                  int bidderID,
                                  ItemType category,
                                  ItemStatus status,
                                  long startTimeStart,
                                  long startTimeEnd,
                                  long endTimeStart,
                                  long endTimeEnd,
                                  boolean startTimeEnabled,
                                  boolean endTimeEnabled,
                                  boolean categoryEnabled,
                                  boolean statusEnabled) {
        super(userID);

        this.itemID = itemID;
        this.sellerID = sellerID;
        this.bidderID = bidderID;
        this.category = category;
        this.status = status;
        this.startTimeStart = startTimeStart;
        this.startTimeEnd = startTimeEnd;
        this.endTimeStart = endTimeStart;
        this.endTimeEnd = endTimeEnd;
        this.startTimeEnabled = startTimeEnabled;
        this.endTimeEnabled = endTimeEnabled;
        this.categoryEnabled = categoryEnabled;
        this.statusEnabled = statusEnabled;
    }

    @Override
    public void execute() {
        sv.getFilteredListings(this);
    }
}

class cl_CreateListing extends ClientMessage {
    String title, description;
    ItemType itemType;
    long startTime, endTime;
    int reservePrice;

    cl_CreateListing(String title, String description, ItemType itemType, long startTime,
                     long endTime, int reservePrice, int userID) {
        super(userID);
        this.title = title;
        this.description = description;
        this.itemType = itemType;
        this.startTime = startTime;
        this.endTime = endTime;
        this.reservePrice = reservePrice;
    }

    @Override
    public void execute() {
        sv.createListing(this);
    }
}

class cl_WithdrawListing extends ClientMessage {
    int itemID;

    public cl_WithdrawListing(int userID, int itemID) {
        super(userID);
        this.itemID = itemID;
    }

    @Override
    public void execute() {
        sv.withdrawListing(this);
    }
}

class cl_PlaceBid extends ClientMessage {
    int itemID, value;

    cl_PlaceBid(int itemID, int bidderID, int value) {
        super(bidderID);
        this.itemID = itemID;
        this.value = value;
    }

    @Override
    public void execute() {
        sv.placeBid(this);
    }
}