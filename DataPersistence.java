import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;

public class DataPersistence {
    private static File userFile = new File("user.auc");
    private static File itemFile = new File("item.auc");
    private static File logFile = new File("log.txt");

    protected static void setFiles(File newUserFile, File newItemFile, File newLogFile) { //Used for unit testing
        userFile = newUserFile;
        itemFile = newItemFile;
        logFile = newLogFile;
    }

    public static void save(Server sv) {
        FileOutputStream userFOS = null;
        FileOutputStream itemFOS = null;
        FileOutputStream logFOS = null;

        ObjectOutputStream userOOS = null;
        ObjectOutputStream itemOOS = null;
        PrintWriter logPW = null;

        try {
            userFOS = new FileOutputStream(userFile);
            itemFOS = new FileOutputStream(itemFile);
            logFOS = new FileOutputStream(logFile);

            userOOS = new ObjectOutputStream(userFOS);
            itemOOS = new ObjectOutputStream(itemFOS);
            logPW = new PrintWriter(logFOS);

            userOOS.writeObject(UserFactory.getUserList());
            itemOOS.writeObject(ItemFactory.getItemList());
            logPW.write(sv.getLogText());

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //Flushing and closing streams
            if (userOOS != null) {
                try {
                    userOOS.flush();
                    userOOS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (itemOOS != null) {
                try {
                    itemOOS.flush();
                    itemOOS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (logPW != null) {
                logPW.flush();
                logPW.close();
            }

            sv.log("Finished saving persistence data.");
        }
    }

    public static void load(Server sv) {
        FileInputStream userFIS = null;
        FileInputStream itemFIS = null;

        ObjectInputStream userOIS = null;
        ObjectInputStream itemOIS = null;

        try {
            userFIS = new FileInputStream(userFile);
            itemFIS = new FileInputStream(itemFile);

            userOIS = new ObjectInputStream(userFIS);
            itemOIS = new ObjectInputStream(itemFIS);
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (userFile.exists() && !userFile.isDirectory()) {
            try {
                ArrayList<User> arr = (ArrayList<User>) userOIS.readObject();
                UserFactory.setUserList(arr);
                sv.log("Loaded user data.");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    userOIS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }

        if (itemFile.exists() && !itemFile.isDirectory()) {

            try {
                ArrayList<Item> arr = (ArrayList<Item>) itemOIS.readObject();
                ItemFactory.setItemList(arr);
                sv.log("Loaded item data.");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    itemOIS.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (logFile.exists() && !logFile.isDirectory()) {
            try {
                sv.insertLogText(FileUtils.readFileToString(logFile));
                sv.log("Loaded log file.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
