import javax.swing.*;
import java.io.Serializable;
import java.net.Socket;

public abstract class Message implements Serializable {
    private transient Socket destinationSocket;
    protected transient ClientLogin cl;
    protected transient Server sv;

    public abstract void execute(); //Uses Command pattern

    //Messages must be constructed with previous one in chain to get destination
    public Message(Message receivedMessage) {
        destinationSocket = receivedMessage.getSocket();
    }

    public Message() {
    }

    public void setClient(ClientLogin cl) {
        this.cl = cl;
    }

    public void setServer(Server sv) {
        this.sv = sv;
    }

    public void setSocket(Socket socket) {
        destinationSocket = socket;
    }

    public Socket getSocket() {
        return destinationSocket;
    }


}

class TestMessage extends Message {
    TestMessage(Message receivedMessage) {
        super(receivedMessage);
    }

    TestMessage() {
    }

    public static void msgBox(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.PLAIN_MESSAGE);
    }

    @Override
    public void execute() {
        System.out.println("TEST MESSAGE RECEIVED!");
        msgBox("This is a message.", "Hi! " + Math.random());
    }
}

class AnotherTestMessage extends Message {
    AnotherTestMessage(Message receivedMessage) {
        super(receivedMessage);
    }

    AnotherTestMessage() {
    }

    public static void msgBox(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.PLAIN_MESSAGE);
    }

    @Override
    public void execute() {
        System.out.println("TEST MESSAGE 2 RECEIVED!");
        msgBox("This is another message.", "Ho! " + Math.random());
    }
}

