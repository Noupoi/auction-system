import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

/**
 * Created by Daniel on 29/04/2014.
 */
public class UserFactory {
    private static ArrayList<User> userList = new ArrayList<User>();

    public static User createUser(String givenName, String familyName, String password) {
        User newUser = null;

        try {
            newUser = new User(givenName, familyName, password, userList.size());
            userList.add(newUser);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return newUser;
    }

    public static User getUser(int userID) {
        if (userID >= 0 && userID < userList.size()) {
            return userList.get(userID);
        } else {
            return null;
        }
    }

    public static ArrayList getUserList() {
        return (ArrayList) userList.clone();
    }

    public static void setUserList(ArrayList<User> newUserList) {
        userList = newUserList;
    }
}

class User implements Serializable{
    private final String givenName, familyName, passwordHash;
    private final int userID;


    private int penaltyPoints;
    //Removals, if implemented in future, must be done by replacing element with null values

    public User(String givenName, String familyName, String password, int userID) throws InvalidKeySpecException, NoSuchAlgorithmException {
        this.givenName = givenName;
        this.familyName = familyName;
        this.passwordHash = PasswordHash.createHash(password);
        this.userID = userID;
        this.penaltyPoints = 0;

    }

    public String givenName() {
        return givenName;
    }

    public String familyName() {
        return familyName;
    }

    public boolean checkPassword(String password) {
        try {
            return PasswordHash.validatePassword(password, passwordHash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return false;
    }

    public int userID() {
        return userID;
    }

    public int getPenaltyPoints() {
        return penaltyPoints;
    }

    public void setPenaltyPoints(int penaltyPoints) {
        this.penaltyPoints = penaltyPoints;
    }

    public void incrementPenaltyPoints(){
        penaltyPoints++;
    }


    @Override
    public String toString() {
        return userID + ": " + givenName + " " + familyName;
    }
}
