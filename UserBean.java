import java.util.ArrayList;
import java.util.List;

public class UserBean {
    private String givenName, familyName;
    private int userID;
    private List<ItemBean> itemBeanList = new ArrayList<ItemBean>();

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public List<ItemBean> getItemBeanList() {
        return itemBeanList;
    }

    /*public void setItemBeanList(List<ItemBean> itemBeanList) {
        this.itemBeanList = itemBeanList;
    }*/

    public void addToItemBeanList(ItemBean itemBean) {
        itemBeanList.add(itemBean);
    }
}
