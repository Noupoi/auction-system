import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Logger;

import static ch.lambdaj.Lambda.*;
import static org.hamcrest.Matchers.*;
import static org.quartz.DateBuilder.futureDate;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * Created by Daniel on 18/04/2014.
 */
public class Server extends JFrame {
    private JPanel serverControls;
    private JPanel serverPanel;
    private JTextArea serverOutputTextArea;
    private JButton buttonReport;
    private JButton saveAndExitButton;
    private ServerComms sc;
    private final Logger logger = Utilities.globalLogger.getLogger(Server.class.getName());
    protected Scheduler scheduler;

    public Server() throws HeadlessException {
        super("Auction System Server");

        $$$setupUI$$$();
        buttonReport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                makeReport();
            }
        });

        saveAndExitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                log("Server is shutting down!");
                DataPersistence.save(Server.this);
                System.exit(1);
            }
        });
    }

    public void init() {
        Utilities.serverInit(this);

        //Create logger
        logger.config("Server logger initiated");

        //Start dispatcher
        sc = new ServerComms();
        (new MessageListener(sc, this)).start();

        DataPersistence.load(this);

        //Start Quartz scheduler
        try {
            SchedulerFactory sf = new StdSchedulerFactory();
            scheduler = sf.getScheduler();
            scheduler.start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        scheduleAllItemRefresh(); //Schedule refreshes for items that have just been loaded in

        try {
            scheduleServerSaveDataJob(); //Schedule periodic save
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        setPreferredSize(new Dimension(700, 600));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(serverPanel);

        pack();
        setLocationRelativeTo(null);

        setVisible(true);

        log("Server started!");
    }

    public void log(String str) {
        serverOutputTextArea.append(Utilities.df.format(Calendar.getInstance().getTime()) + ": " + str + "\n");
        logger.info(str);
    }


    public void login(cl_Login m) {
        User u = UserFactory.getUser(m.userID);

        if (u != null && u.checkPassword(new String(m.password))) {
            //Correct password
            sc.sendMessage(new sv_LoginResult(m, m.userID, true));
            log("Successful login: " + u);
        } else {
            //Incorrect password
            sc.sendMessage(new sv_LoginResult(m, m.userID, false));
            log("Failed login: " + u);
        }
    }

    public void register(cl_Register registerMsg) {

        if (registerMsg.familyName != null && registerMsg.givenName != null && registerMsg.password != null) {
            User newUser = UserFactory.createUser(registerMsg.givenName, registerMsg.familyName, registerMsg.password);
            log("Created user: " + newUser);
            //Notify client
            sc.sendMessage(new sv_RegisterResult(registerMsg, newUser.userID()));
        } else {
            logger.severe("User creation failed!");
        }
    }

    public void getPenaltyPoints(cl_GetPenaltyPoints message) {

        sc.sendMessage(new sv_SendNotification(message, "You have " +
                UserFactory.getUser(message.userID).getPenaltyPoints() + " penalty point(s)."));
    }

    public void getListings(cl_GetListings msgGetListings) {

        log("Item list requested by user " + msgGetListings.userID);
        sv_ReturnListings newMsg = new sv_ReturnListings(msgGetListings, ItemFactory.getItemList());
        sc.sendMessage(newMsg);
    }

    public void getFilteredListings(cl_GetFilteredListings ms) {
        List<Item> items = ItemFactory.getItemList();

        //Filter listings. lambdaj was used for enhanced readability.

        if (ms.itemID != -1) {//Item ID
            items = filter(having(on(Item.class).getItemID(), equalTo(ms.itemID)), items);
        }

        if (ms.sellerID != -1) {//Seller ID
            items = filter(having(on(Item.class).getVendorID(), equalTo(ms.sellerID)), items);
        }

        if (ms.bidderID != -1) {//Bidder ID
            items = filter(having(on(Item.class).isBidder(ms.bidderID), equalTo(true)), items);
        }

        if (ms.categoryEnabled) {//Category. lambdaj cannot work with enums, so manual iterations were done.
            items = ItemFactory.filterByCategory(items, ms.category);
        }

        if (ms.statusEnabled) { //Status
            items = ItemFactory.filterByStatus(items, ms.status);
        }

        if (ms.startTimeEnabled) { //Start time
            items = filter(having(on(Item.class).getStartTime(), greaterThanOrEqualTo(ms.startTimeStart)), items);
            items = filter(having(on(Item.class).getStartTime(), lessThanOrEqualTo(ms.startTimeEnd)), items);
        }

        if (ms.endTimeEnabled) { //End time
            items = filter(having(on(Item.class).getEndTime(), greaterThanOrEqualTo(ms.endTimeStart)), items);
            items = filter(having(on(Item.class).getEndTime(), lessThanOrEqualTo(ms.endTimeEnd)), items);
        }


        ArrayList<Item> arr = new ArrayList<Item>(items);
        log("Filtered list requested by user " + ms.userID);
        sc.sendMessage(new sv_ReturnListings(ms, arr));

    }


    public void createListing(cl_CreateListing msg) {
        Date endTime = new Date(msg.endTime);
        Date now = new Date();

        if (UserFactory.getUser(msg.userID).getPenaltyPoints() > 2) { //Check penalty points
            log("User " + msg.userID + " tried to create a listing but has too many penalty points!");
            sendNotification(msg, "Could not create listing. You have too many penalty points!");
        } else if (endTime.after(now)) {
            Item newItem = ItemFactory.createItem(msg.userID, msg.title, msg.description,
                    msg.itemType, msg.startTime, msg.endTime, msg.reservePrice);

            scheduleItemRefresh(newItem.getItemID());

            log("Created new listing with id " + newItem.getItemID());
            sendNotification(msg, "Listing created!");
        } else {
            sendNotification(msg, "Listing was not created due to invalid end time!");
        }

    }

    public void withdrawListing(cl_WithdrawListing msg) {
        Item item = ItemFactory.getItem(msg.itemID);

        //Verification
        if (msg.userID == item.getVendorID() &&
                (item.getStatus() == ItemStatus.PENDING || item.getStatus() == ItemStatus.ACTIVE)) {

            item.setStatus(ItemStatus.WITHDRAWN);

            User user = UserFactory.getUser(msg.userID);
            if (item.getTopBidValue() >= item.getReservePrice()) { //If reserve price has been met
                user.incrementPenaltyPoints();
            }

            sendNotification(msg, "Item " + item.getItemID() + " withdrawn. You now have "
                    + user.getPenaltyPoints() + " penalty point(s).");
        }

    }

    public void placeBid(cl_PlaceBid msg) {

        Item it = ItemFactory.getItem(msg.itemID);
        if (it.placeBid(msg.value, msg.userID)) { //If successful
            log("Bid placed on item " + msg.itemID + " by user " + msg.userID + " for " + msg.value);
            sendNotification(msg, "Bid placed. Refresh listings to see updated info!");
        } else {
            sendNotification(msg, "Bid could not be placed!");
        }

    }

    public void sendNotification(Message msg, String notification) {
        sc.sendMessage(new sv_SendNotification(msg, notification));
    }

    public void scheduleAllItemRefresh() {
        ArrayList<Item> arr = ItemFactory.getItemList();

        for (Item i : arr) {
            ItemStatus status = i.refreshStatus(); //Refresh
            if (status == ItemStatus.PENDING || status == ItemStatus.ACTIVE) { //Schedule
                scheduleItemRefresh(i.getItemID());
            }
        }
    }

    public void scheduleItemRefresh(int itemID) {
        Item item = ItemFactory.getItem(itemID);

        try {
            if (item.getStatus() == ItemStatus.PENDING) { //Schedule check at start time
                JobDetail jobStartTime = newJob(UpdateListingStatusJob.class)
                        .withIdentity(itemID + "StartTimeJob", "group1")
                        .usingJobData("itemID", item.getItemID())
                        .build();

                Trigger triggerStartTime = newTrigger()
                        .withIdentity(itemID + "StartTimeTrigger")
                        .startAt(new Date(item.getStartTime()))
                        .forJob(itemID + "StartTimeJob", "group1")
                        .build();

                scheduler.scheduleJob(jobStartTime, triggerStartTime);
            }

            if (item.getStatus() == ItemStatus.ACTIVE || item.getStatus() == ItemStatus.PENDING) {
                //Schedule check at end time
                JobDetail jobEndTime = newJob(UpdateListingStatusJob.class)
                        .withIdentity(itemID + "EndTimeJob", "group1")
                        .usingJobData("itemID", item.getItemID())
                        .build();

                Trigger triggerEndTime = newTrigger()
                        .withIdentity(itemID + "EndTimeTrigger")
                        .startAt(new Date(item.getEndTime()))
                        .forJob(itemID + "EndTimeJob", "group1")
                        .build();

                scheduler.scheduleJob(jobEndTime, triggerEndTime);
            }

        } catch (SchedulerException e) {
            e.printStackTrace();
            log(e.toString());
        }

        log("Scheduling refresh on item " + itemID);
    }

    public void scheduleServerSaveDataJob() throws SchedulerException {

        JobDetail jobSaveData = newJob(ServerSaveDataJob.class)
                .withIdentity("SaveDataJob", "group2")
                .build();

        Trigger triggerStartTime = newTrigger()
                .withIdentity("SaveDataJob")
                .startAt(futureDate(5, DateBuilder.IntervalUnit.MINUTE))
                .withSchedule(simpleSchedule()
                        .withIntervalInMinutes(5)
                        .repeatForever())
                .forJob("SaveDataJob", "group2")
                .build();

        scheduler.scheduleJob(jobSaveData, triggerStartTime);
        log("Scheduled automatic data saves.");
    }

    public void refreshItemStatus(int itemID) {
        //Refresh status, and if completed, notify winning bidder
        Item itm = ItemFactory.getItem(itemID);

        if (itm.refreshStatus() == ItemStatus.COMPLETED) {
            Message lastMsg = Utilities.getMostRecentMessage(itm.getTopBidder());

            if (lastMsg != null) {
                sc.sendMessage(new sv_SendNotification(
                        lastMsg, "You have won listing " + itm.getItemID() + " " + itm.getTitle() + "!"));
            }

        }
    }

    public void makeReport() {
        ArrayList<UserBean> userBeans = UserBeanList.getUserBeanList();

        JRBeanCollectionDataSource userDataSource = new JRBeanCollectionDataSource(userBeans);

        try {
            JasperReport masterReport = JasperCompileManager.compileReport("AuctionSystem.Jrxml");
            JasperReport subReport = JasperCompileManager.compileReport("AuctionSystemSubreport.Jrxml");

            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("subreportParameter", subReport);

            JasperPrint jasperPrint = JasperFillManager.fillReport(masterReport, parameters, userDataSource);
            JasperViewer jasperViewer = new JasperViewer(jasperPrint, false);
            jasperViewer.setVisible(true);

        } catch (JRException e) {
            e.printStackTrace();
        }

    }

    public String getLogText() {
        return serverOutputTextArea.getText();
    }

    public void insertLogText(String text) {
        try {
            serverOutputTextArea.getDocument().insertString(0, text, null);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }

    public void setLogText(String text) {
        serverOutputTextArea.setText(text);
    }

    private void createUIComponents() {
        serverOutputTextArea = new JTextArea();
        //Set auto scrolling in log
        DefaultCaret caret = (DefaultCaret) serverOutputTextArea.getCaret();
        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

    }

    public static void main(String[] args) {
        Utilities.init();

        Server s = new Server();
        s.init();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        serverPanel = new JPanel();
        serverPanel.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        serverControls = new JPanel();
        serverControls.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        serverPanel.add(serverControls, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        serverControls.add(spacer1, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        buttonReport = new JButton();
        buttonReport.setHorizontalAlignment(0);
        buttonReport.setText("Generate report");
        serverControls.add(buttonReport, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        saveAndExitButton = new JButton();
        saveAndExitButton.setText("Save and exit");
        serverControls.add(saveAndExitButton, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JScrollPane scrollPane1 = new JScrollPane();
        serverPanel.add(scrollPane1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        serverOutputTextArea.setEditable(false);
        serverOutputTextArea.setText("");
        scrollPane1.setViewportView(serverOutputTextArea);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return serverPanel;
    }
}

