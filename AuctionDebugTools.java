import javax.swing.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Daniel on 30/04/2014.
 */
public class AuctionDebugTools {
    public static void main(String[] args) {
        Utilities.init();

        //createDummyUsers();
        //createTestListings();

        showClientAndServer();

        //testCommsOnly();
        //testServerGUI();
        //testServerAndClientGUI();

        //displayItemListing();
        //displayItemDetailedDisplay();

        //testBeans();

    }

    public static void testCommsOnly() {
        ServerComms sc = new ServerComms();
        ClientComms cc = new ClientComms();

        cc.sendMessage(new TestMessage());
        Message msg = sc.receiveMessage();
        msg.execute();

        Message reply = new AnotherTestMessage(msg);

        sc.sendMessage(reply);
        cc.receiveMessage().execute();

        System.out.println("Test done!");
        System.exit(0);
    }

    public static void testServerGUI() {
        ClientComms cc = new ClientComms();
        Server server = new Server();
        server.init();

        cc.sendMessage(new TestMessage());
    }

    public static void testServerAndClientGUI() {
        Server server = new Server();
        ClientLogin clientLogin = new ClientLogin();

        server.init();
        clientLogin.init();
    }

    public static void showClientAndServer() {
        Client.main(null);
        Server.main(null);
    }

    public static void createDummyUsers() {
        UserFactory.createUser("John", "Smith", "pass");
        UserFactory.createUser("David", "Ross", "pass");
        UserFactory.createUser("Jade", "Watson", "pass");
        UserFactory.createUser("Isaac", "Marsden", "pass");
        UserFactory.createUser("Jude", "Nash", "pass");
    }

    public static void createTestListings(){
        GregorianCalendar gc = new GregorianCalendar(Locale.UK);
        long now = gc.getTimeInMillis();

        gc.add(Calendar.MINUTE, 1);
        long soon = gc.getTimeInMillis();

        //Successful listing 1
        Item item1 = ItemFactory.createItem(0, "Chair", "A chair", ItemType.HOME_AND_GARDEN, now, soon, 100);
        item1.placeBid(1300, 1);
        item1.placeBid(1400, 2);
        item1.setStatus(ItemStatus.COMPLETED);

        //Successful listing 1
        Item item2 = ItemFactory.createItem(1, "Pet duck", "A real pet duck!", ItemType.COLLECTIBLES_AND_ANTIQUES, now, soon, 200);
        item2.placeBid(500, 1);
        item2.placeBid(800, 2);
        item2.setStatus(ItemStatus.COMPLETED);

        //Failed listing
        Item item3 = ItemFactory.createItem(2, "Something", "A mystery", ItemType.OTHER, now, soon, 5000);
        item3.placeBid(1000, 1);
        item3.placeBid(2000, 2);
        item3.setStatus(ItemStatus.FAILED);

        //Withdrawn listing
        Item item4 = ItemFactory.createItem(2, "Mouldy cheese", "Why not?", ItemType.OTHER, now, soon, 1);
        item4.setStatus(ItemStatus.WITHDRAWN);
    }

    public static void testBeans() { //Prints out information on each bean
        List<UserBean> userBeans = UserBeanList.getUserBeanList();
        System.out.println(userBeans);

        for (UserBean ub : userBeans) {
            System.out.println(ub.getUserID() + "," + ub.getItemBeanList());
            if (ub.getItemBeanList().size() > 0) {
                for (ItemBean ib : ub.getItemBeanList()) {
                    System.out.println("Item bean " + ib.getItemID() + " " + ib.getTitle());
                }
            }
        }
    }

    public static void displayItemListing() {
        JFrame jf = new JFrame();
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ListingsDisplay ld = new ListingsDisplay(null);
        ld.init(ItemFactory.getItemList());

        jf.setContentPane(ld);
        jf.pack();
        jf.setVisible(true);
    }

    public static void displayItemDetailedDisplay() {
        JFrame jf = new JFrame();
        jf.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        ItemDetailedDisplay idd = new ItemDetailedDisplay(null);
        idd.init(ItemFactory.getItem(0));

        jf.setContentPane(idd);
        jf.pack();
        jf.setVisible(true);
    }

}
