import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Created by Daniel on 17/04/2014.
 */
public class ItemFactory {
    private static ArrayList<Item> itemList = new ArrayList<Item>();

    public static Item createItem(int vendorID, String title, String description, ItemType category,
                                  long startTime, long endTime, int reservePrice) {

        Item newItem = new Item(itemList.size(), vendorID, title, description,
                category, startTime, endTime, reservePrice);

        itemList.add(newItem);
        return newItem;
    }

    public static ArrayList getItemList() {
        return (ArrayList) itemList.clone();
    }

    public static void setItemList(ArrayList<Item> newItemList) {
        itemList = newItemList;
    }

    public static Item getItem(int itemID) {
        if (itemID >= 0 && itemID < itemList.size()) {
            return itemList.get(itemID);
        } else {
            return null;
        }
    }

    public static List filterByStatus(List<Item> old, ItemStatus itemStatus) {
        List<Item> result = new ArrayList();

        for (Item i : old) {
            if (i.getStatus() == itemStatus) result.add(i);
        }

        return result;
    }

    public static List filterByCategory(List<Item> old, ItemType category) {
        List<Item> result = new ArrayList();

        for (Item i : old) {
            if (i.getCategory() == category) result.add(i);
        }

        return result;
    }
}


enum ItemType {
    COLLECTIBLES_AND_ANTIQUES("Collectibles and antiques"),
    ELECTRONICS("Electronics"),
    FASHION("Fashion"),
    HOME_AND_GARDEN("Home and garden"),
    JEWELLERY_AND_WATCHES("Jewellery and watches"),
    MOTORS("Motors"),
    SPORTING_GOODS("Sporting goods"),
    TOYS_AND_GAMES("Toys and games"),
    OTHER("Other");

    private ItemType(String text) {
        this.text = text;
    }

    private final String text;

    @Override
    public String toString() {
        return text;
    }
}

enum ItemStatus {
    PENDING, ACTIVE, COMPLETED, FAILED, WITHDRAWN
}

class Item implements Serializable {
    private final int itemID, vendorID;
    private String title, description;
    private ItemType category;
    private long startTime, endTime;
    private int reservePrice; //Stored in pennies
    private ArrayList<Bid> bids;
    private ItemStatus status = ItemStatus.PENDING;

    public Item(int itemID, int vendorID, String title,
                String description, ItemType category,
                long startTime, long endTime, int reservePrice) {

        this.itemID = itemID;
        this.vendorID = vendorID;
        this.title = title;
        this.description = description;
        this.category = category;
        this.startTime = startTime;
        this.endTime = endTime;
        this.reservePrice = reservePrice;

        this.bids = new ArrayList<Bid>();

        //Set status based on start time
        Date startDate = new Date(startTime);
        Date nowDate = new Date();

        if (nowDate.after(startDate)) {
            status = ItemStatus.ACTIVE;
        } else {
            status = ItemStatus.PENDING;
        }
    }

    public synchronized ItemStatus getStatus() {
        return status;
    }

    public synchronized void setStatus(ItemStatus status) {
        this.status = status;
    }

    public ItemStatus refreshStatus() {
        Date now = new Date();
        Date startDate = new Date(startTime);
        Date endDate = new Date(endTime);

        synchronized (this) {
            if (startDate.before(now)) {  //Start time reached
                if (status == ItemStatus.PENDING) {
                    status = ItemStatus.ACTIVE;
                } else if (status == ItemStatus.ACTIVE && now.after(endDate)) { //If active and end time reached
                    if (reservePrice > getTopBidValue() || getBids().size() == 0) { //Listing failed
                        status = ItemStatus.FAILED;
                    } else {
                        status = ItemStatus.COMPLETED;
                    }
                }
            }
        }


        return status;
    }

    public boolean placeBid(int value, int bidderID) {
        if (value <= getTopBidValue() || status != ItemStatus.ACTIVE) {
            return false;
        } else {
            bids.add(new Bid(value, bidderID));
            Collections.sort(bids);
            return true;
        }
    }

    private Bid getTopBid() {
        if (bids.size() > 0) {
            return bids.get(bids.size() - 1);
        } else {
            return null;
        }
    }

    public int getTopBidValue() {
        if (getTopBid() == null) {
            return 0;
        } else {
            return getTopBid().getValue();
        }

    }

    public int getTopBidder() {
        if (getTopBid() == null) {
            return -1;
        } else {
            return getTopBid().getBidder().userID();
        }
    }

    public boolean isBidder(int userID) {
        boolean result = false;

        for (Bid b : bids) {
            if (b.getBidderID() == userID){
                result = true;
                break;
            }
        }

        return result;
    }

    public int getVendorID() {
        return vendorID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemType getCategory() {
        return category;
    }

    public void setCategory(ItemType category) {
        this.category = category;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getReservePrice() {
        return reservePrice;
    }

    public void setReservePrice(int reservePrice) {
        this.reservePrice = reservePrice;
    }

    public ArrayList getBids() {
        return (ArrayList<Bid>) bids.clone();
    }

    public int getItemID() {
        return itemID;
    }

    @Override
    public String toString() {
        return "Item " + itemID + " highest bid " + getTopBidValue() + " seller " + vendorID;
    }

}

class Bid implements Comparable, Serializable {
    private int value;
    private int bidderID;

    Bid(int value, int bidderID) {
        this.value = value;
        this.bidderID = bidderID;
    }

    public int getValue() {
        return value;
    }

    public User getBidder() {
        return UserFactory.getUser(bidderID);
    }

    public int getBidderID() {
        return bidderID;
    }

    @Override
    public int compareTo(Object o) {
        Bid other = (Bid) o;
        return value - other.getValue();
    }
}
