import java.util.ArrayList;

/**
 * Created by Daniel on 04/05/2014.
 */
public abstract class ServerMessage extends Message {
    ServerMessage(Message message) {
        super(message);
    }

}

class sv_LoginResult extends ServerMessage {
    final boolean success;
    final int userID;

    sv_LoginResult(Message m, int userID, boolean success) {
        super(m);
        this.userID = userID;
        this.success = success;
    }

    @Override
    public void execute() {
        if (success) {
            cl.successLogin(userID);
        } else {
            cl.failLogin();
        }
    }
}

class sv_RegisterResult extends ServerMessage {
    int newID;

    sv_RegisterResult(Message message, int userID) {
        super(message);
        this.newID = userID;
    }

    @Override
    public void execute() {
        cl.getClientRegister().registerSuccess(newID);
    }
}

class sv_ReturnListings extends ServerMessage {
    ArrayList<Item> arr;

    sv_ReturnListings(Message message, ArrayList<Item> arr) {
        super(message);
        this.arr = arr;
    }

    @Override
    public void execute() {
        cl.getClient().updateListingDisplay(arr);
    }
}

class sv_SendNotification extends ServerMessage{
    final String notification;

    sv_SendNotification(Message msg, String notification) {
        super(msg);
        this.notification = notification;
    }

    @Override
    public void execute() {
        cl.getClient().showNotification(notification);
    }
}